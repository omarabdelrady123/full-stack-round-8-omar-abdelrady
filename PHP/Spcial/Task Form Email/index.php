<?php
require_once 'config/config.php';
//require_once 'models/User.php';
//$user = new User();
require_once 'layout/_header.php';
// content page

// routing using SERVER
if (empty($_SERVER['PATH_INFO'])) {
    require_once './view/home.php';
} else {
    $page =  VIEW_DIR . $_SERVER['PATH_INFO'] . '.php';
    if (file_exists($page)) {
        require_once $page;
    } else {
        require_once '404.php';
    }
}

// routing using GET
/*if (empty($_GET['page'])) {
    require_once 'home.php';
} else {
    $page = $_GET['page'] . '.php';
    if (file_exists($page)) {
        require_once $page;
    } else {
        require_once '404.php';
    }
}*/
// end content page
require_once 'layout/_footer.php';