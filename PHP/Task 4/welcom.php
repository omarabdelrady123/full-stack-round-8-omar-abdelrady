<?php
    session_start();
?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="CSS/bootstrap.min.css">
    <title>Welcome</title>
</head>
<body>
    <div class="container">
        <div class="row mt-5 ">

            <div class="alert alert-success mt-5 col-12" role="alert">
                <strong>Hello M.</strong><?= $_SESSION['v12']?>
            </div>

            <h2 class="mt-5 col-12 text-success">Yor Data (:</h2>

            <h5 class="col-4">Sim Facebook :</h5> <a href="<?php echo $_SESSION['v1']?>" class="h6 col-8    ">Facebook</a>
            <h5 class="col-4">Sim Instgram :</h5> <a href="<?= $_SESSION['v2']?>" class="h6 col-8">Instgram</a>
            <h5 class="col-4">Date of birth :</h5>
            <h6 class="col-8"><?= $_SESSION['v3']?></h6>
            <h5 class="col-4">Educations :</h5>
            <h6 class="col-8"><?= $_SESSION['v4']?></h6>
            <h5 class="col-4">BIO :</h5>
            <h6 class="col-8"><?= $_SESSION['v5']?></h6>
            <h5 class="col-4">Price :</h5>
            <h6 class="col-8"><?= $_SESSION['v6']?></h6>
            <h5 class="col-4">Points :</h5>
            <h6 class="col-8"><?= $_SESSION['v7']?></h6>
            <h5 class="col-4">Tattoos :</h5>
            <h6 class="col-8"><?= $_SESSION['v8']?></h6>
            <h5 class="col-4">Scars :</h5>
            <h6 class="col-8"><?= $_SESSION['v9']?></h6>
            <h5 class="col-4">Talents :</h5>
            <h6 class="col-8"><?= $_SESSION['v10']?></h6>
            <h5 class="col-4">Created at :</h5>
            <h6 class="col-8"><?= $_SESSION['v11']?></h6>
            <h5 class="col-4">User :</h5>
            <h6 class="col-8"><?= $_SESSION['v12']?></h6>
            <h5 class="col-4">Avatar :</h5>
            <h6 class="col-8"><?= $_SESSION['v13']?></h6>
            <h5 class="col-4">Model Category :</h5>
            <h6 class="col-8"><?= $_SESSION['v14']?></h6>
            <h5 class="col-4">Nationality :</h5>
            <h6 class="col-8"><?= $_SESSION['v15']?></h6>
            <h5 class="col-4">Height :</h5>
            <h6 class="col-8"><?= $_SESSION['v16']?></h6>
            <h5 class="col-4">Skin Tone :</h5>
            <h6 class="col-8"><?= $_SESSION['v17']?></h6>
            <h5 class="col-4">Weight :</h5>
            <h6 class="col-8"><?= $_SESSION['v18']?></h6>
            <h5 class="col-4">Hair Type :</h5>
            <h6 class="col-8"><?= $_SESSION['v19']?></h6>
            <h5 class="col-4">Eye Color :</h5>
            <h6 class="col-8"><?= $_SESSION['v20']?></h6>
            <h5 class="col-4">Body :</h5>
            <h6 class="col-8"><?= $_SESSION['v21']?></h6>
            <h5 class="col-4">Gander :</h5>
            <h6 class="col-8"><?= $_SESSION['v22']?></h6>
            <h5 class="col-4">Lang :</h5>
            <h6 class="col-8"><?= $_SESSION['v23']?></h6>
            <h5 class="col-4">auditions :</h5>
            <h6 class="col-8"><?= $_SESSION['v24']?></h6>
            <h5 class="col-4">calendars :</h5>
            <h6 class="col-8"><?= $_SESSION['v25']?></h6>
            <h5 class="col-4">Lang 2 :</h5>
            <h6 class="col-8"><?= $_SESSION['v26']?></h6>
            <h5 class="col-4">work experience :</h5>
            <h6 class="col-8"><?= $_SESSION['v27']?></h6>

        </div>

    </div>
    <link rel="stylesheet" href="JS/jquery-3.4.1.min.js">
    <link rel="stylesheet" href="JS/popper.min.js">
    <link rel="stylesheet" href="JS/bootstrap.min.js">
</body>
</html>





