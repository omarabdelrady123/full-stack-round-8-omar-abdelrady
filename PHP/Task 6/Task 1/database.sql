CREATE DATABASE task_1;

USE task_1;

CREATE TABLE users(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(15) NOT NULL,
    scoure  INT,
    max_scoure INT
);
INSERT INTO users (name, scoure, max_scoure)

VALUES ('Omar Ashraf',        '580'   , '1000'),
       ('Abdelrahman Amr',    '565'   , '1000'),
       ('Mostafa Rafea',      '715'   , '1000'),
       ('Marco Monier',       '260'   , '1000'),
       ('Sara Galal',         '315'   , '1000'),
       ('Omnai Mahmoud',      '175'   , '1000'),
       ('William',            '430'   , '1000'),
       ('Yousef Ibrahim',     '440'   , '1000'),
       ('Mohamed Baligh',     '40'    , '1000'),
       ('Ibrahim Morsi',      '365'   , '1000'),
       ('Karim Fekry',        '255'   , '1000'),
       ('Marwan',             '65'    , '1000'),
       ('Mohamed Tark',       '65'    , '1000'),
       ('Omar Abdelrady',     '715'   , '1000'),
       ('Shrief Mahmoud',     '225'   , '1000'),
       ('AbdAllh',            '5'     , '1000')
