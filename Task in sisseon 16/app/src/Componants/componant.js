import React from 'react';


const Comment = ({row, increment}) => {

    return (
        <div className="col-md-12 ">
            <div className="card mb-3">
                <div className="card-body">

                    <h5 className="card-title">{row.name}</h5>
                    <p className="card-text">
                        {row.body}
                    </p>
                    <span>
                        {row.like}
                    </span>

                </div>
            </div>
        </div>
    );
};

export default Comment;